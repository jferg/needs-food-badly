'use strict';

import { AxiosRequestConfig } from 'axios';
import * as ToughCookie from 'tough-cookie';
import NFBProvider from './nfb.provider';
import Address from './models/address';

export default class nfb {
  private cookieJar: ToughCookie;

  constructor(public service: string) {

  }

  registerProviderPlugin(pluginInstance: NFBProvider): void {

  }

  getProvidersForAddress(address: Address, type: 'delivery' | 'carryout' | 'any') {

  }

  getDeliveryTimeOptionsForLocation(location: ProviderLocation) {

  }
}
