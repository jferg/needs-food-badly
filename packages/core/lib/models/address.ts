/**
 * Object to represent a street address, theoretically anywhere in the world.
 */

export default class Address {
  providedText: string = '';
  line1: string = '';
  line2: string = '';
  line3: string = '';
  line4: string = '';
  locality: string = '';
  region: string = '';
  postalCode: string = '';
  country: string = '';
  latitude?: number;
  longitude?: number;
}
