import Address from './address';
import BusinessHours from './business.hours';

/**
 * Represents an individual instance of a provider
 */
export class ProviderLocation {
  locationId: string;
  locationName?: string;
  locationDetail?: string;
  locationDetail2?: string;
  providerLocationId?: string;

  locationAddress?: Address;
  locationHours?: BusinessHours[];

  constructor(locationId: string, locationAddress?: Address) {
    this.locationId = locationId;
    this.locationAddress = locationAddress;
  }

  setHours(openTimes: BusinessHours[]): ProviderLocation {

    return this;
  }

}